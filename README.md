# computer aided design

Start a shell with

    nix-shell

Then start the jupyter notebook server

    jupyter lab

Set the url (w/ token) as codium's remote jupyter server.
